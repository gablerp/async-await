﻿using ConsoleApp.Utilities;
using System;
using System.Threading.Tasks;

namespace ConsoleApp
{
    /// <summary>
    /// This is demo example to show async/await method usage.
    /// </summary>
    public static class Program
    {
        public static async Task Main()
        {
            var arrayOP = new ArrayOperations();
            int[] generated = new int[10];
            int[] mulitplyed = new int[10];
            int[] sorted = new int[10];
            int avg = 0;
            try
            {
                generated = await arrayOP.GetArrayWithRandomNumbersAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception occuerd after trying to generate an array with random numbers:");
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("An array of intigers was genereted sucessfully:");
                for (int i = 0; i < generated.Length; i++)
                {
                    Console.WriteLine($"Array[{i}] = {generated[i]}");
                }
            }

            try
            {
                mulitplyed = await arrayOP.MultiplyArrayByRandomNumberAsync(generated);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception occuerd after trying to multiply an array by random number:");
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("An array of genereted intigers was multiplyed by a random number:");
                for (int i = 0; i < mulitplyed.Length; i++)
                {
                    Console.WriteLine($"Array[{i}] = {mulitplyed[i]}");
                }
            }

            try
            {
                sorted = await arrayOP.SortArrayAscendingAsync(mulitplyed);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception occuerd after trying to sort an array:");
                Console.WriteLine(e.Message);
            }
            finally 
            {
                Console.WriteLine("An array of mulitplyed intigers was sorted ascending:");
                for (int i = 0; i < sorted.Length; i++)
                {
                    Console.WriteLine($"Array[{i}] = {sorted[i]}");
                }
            }

            try
            {
                avg = await arrayOP.GetAverageFormArrayAsync(sorted);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception occuerd after trying to calcualte an average:");
                Console.WriteLine(e.Message);

            }
            finally
            {
                Console.WriteLine("An average from all integers of an array:");
                Console.WriteLine(avg);
            }

            Console.ReadLine();
        }
    }
}
