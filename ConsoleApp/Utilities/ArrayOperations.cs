﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Utilities
{
    /// <summary>
    /// This class contains methods for array operations.
    /// </summary>
    public class ArrayOperations
    {

        /// <summary>
        /// Method that returns array of 10 random integers - value of each integer is between 0 - 1000.
        /// </summary>
        /// <returns>Returns array of 10 random integers.</returns>
        public int[] GetArrayWithRandomNumbers()
        {
            var list = new List<int>();
            var rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                list.Add(rnd.Next(0, 1000));
            }
            return list.ToArray();
        }

        /// <summary>
        /// Async version of method that returns array of 10 random integers - value of each integer is between 0 - 1000.
        /// </summary>
        /// <returns>Returns array of 10 random integers.</returns>
        public async Task<int[]> GetArrayWithRandomNumbersAsync()
        {
            await Task.Delay(0);
            return GetArrayWithRandomNumbers();
        }

        /// <summary>
        /// Method that multiply given array of 10 integers by random genreted number (from 0 to 1000).
        /// </summary>
        /// <param name="array">Input array of 10 integers.</param>
        /// <returns>Returns an array of 10 integers.</returns>
        public int[] MultiplyArrayByRandomNumber(int[] array)
        {
            ValidateArray(array);
            var rnd = new Random();
            int multiplyer = rnd.Next(0, 1000);
            for (int i = 0; i < array.Length; i++)
            {
                array[i] *= multiplyer;
            }
            return array;
        }

        /// <summary>
        /// Async version of method that multiply given array of 10 integers by random genreted number (from 0 to 1000).
        /// </summary>
        /// <param name="array">Input array of 10 integers.</param>
        /// <returns>Returns an array of 10 integers.</returns>
        public async Task<int[]> MultiplyArrayByRandomNumberAsync(int[] array)
        {
            await Task.Delay(0);
            return MultiplyArrayByRandomNumber(array);
        }

        /// <summary>
        /// Method that calculate an average from array of 10 integers.
        /// </summary>
        /// <param name="array">Input array of 10 integers.</param>
        /// <returns>Returns average integer value.</returns>
        public int GetAverageFormArray(int[] array)
        {
            ValidateArray(array);
            int sum = 0;
            for (int i = 0;i < array.Length;i++)
            {
                sum += array[i];
            }
            return sum / array.Length;
        }

        /// <summary>
        /// Async version of method that calculate an average from array of 10 integers.
        /// </summary>
        /// <param name="array">Input array of 10 integers.</param>
        /// <returns>Returns average integer value.</returns>
        public async Task<int> GetAverageFormArrayAsync(int[] array)
        {
            await Task.Delay(0);
            return GetAverageFormArray(array);
        }

        /// <summary>
        /// Method that sorts ascending given array of 10 integers.
        /// </summary>
        /// <param name="array">Input array of 10 integers.</param>
        /// <returns>Returns sorted array of 10 integers.</returns>
        public int[] SortArrayAscending(int[] array)
        {
            ValidateArray(array);
            return array.OrderBy(x => x).ToArray();
        }

        /// <summary>
        /// Async version of method that sorts ascending given array of 10 integers.
        /// </summary>
        /// <param name="array">Input array of 10 integers.</param>
        /// <returns>Returns sorted array of 10 integers.</returns>
        public async Task<int[]> SortArrayAscendingAsync(int[] array)
        {
            await Task.Delay(0);
            return SortArrayAscending(array);
        }

        private void ValidateArray(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException("Array is empty.", nameof(array));
            }

            if (array.Length != 10)
            {
                throw new ArgumentException("Lenght of array is not equal 10.", nameof(array));
            }

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(array), "One of array entry value is less than zero.");
                }
                if (array[i] > 1000000)
                {
                    throw new ArgumentOutOfRangeException(nameof(array), "One of array entry value is more than 1000.");
                }
            }
        }

    }
}
