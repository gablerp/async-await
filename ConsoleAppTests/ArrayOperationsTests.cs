﻿using ConsoleApp.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppTests
{
    [TestFixture]
    public class ArrayOperationsTests
    {
        /// <summary>
        /// Tests if MultiplyArrayByRandomNumber method throws ArgumentNullException when input parameter is null.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        [Test]
        [TestCase(null)]
        public void MultiplyArrayByRandomNumberInputIsNull(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentNullException>(() => arrayOperation.MultiplyArrayByRandomNumber(array));
        }

        /// <summary>
        /// Tests if MultiplyArrayByRandomNumber method throws ArgumentException when input parameter is invalid or empty.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        [Test]
        [TestCase(new int[] {0, 1, 2, 3, 4, 5, 6, 7})]
        [TestCase(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })]
        [TestCase(new int[] { })]
        public void MultiplyArrayByRandomNumberInputIsInvalid(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentException>(() => arrayOperation.MultiplyArrayByRandomNumber(array));
        }

        /// <summary>
        /// Tests if MultiplyArrayByRandomNumber method throws ArgumentOutOfRangeException when input array values are less than 0 or greater than 1000000.
        /// </summary>
        /// <param name="array">Input parameter - array of integers.</param>
        [Test]
        [TestCase(new int[] { 0, 1, 2, -3, 4, 5, 6, 7, 8, 9})]
        [TestCase(new int[] { 0, 1, 2, 3, 4, 5, 6, 70000000, 8, 9 })]
        public void MultiplyArrayByRandomNumberInputElementsAreOutOfRange(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentOutOfRangeException>(() => arrayOperation.MultiplyArrayByRandomNumber(array));
        }

        /// <summary>
        /// Tests if  MultiplyArrayByRandomNumber method returns array of 10 integers and if value of each is greater or equal than before.
        /// </summary>
        /// <param name="array">Input parameter - array of integers.</param>
        /// <param name="arrayUnchaged">Second array that holds reference values of input parameter.</param>
        [Test]
        [TestCase(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        [TestCase(new int[] { 45, 181, 362, 873, 94, 5, 16, 877, 89, 109 }, new int[] { 45, 181, 362, 873, 94, 5, 16, 877, 89, 109 })]
        public void MultiplyArrayByRandomNumberInputIsValid(int[] array, int[]arrayUnchaged)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            var actual = arrayOperation.MultiplyArrayByRandomNumber(array);
            Assert.AreEqual(10, actual.Length);
            for (int i = 0; i < 10; i++)
            {
                Assert.GreaterOrEqual(actual[i], arrayUnchaged[i]);
            }
        }

        /// <summary>
        /// Tests if GetAverageFormArray method throws ArgumentNullException when input parameter is null.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        [Test]
        [TestCase(null)]
        public void GetAverageFormArrayInputIsNull(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentNullException>(() => arrayOperation.GetAverageFormArray(array));
        }

        /// <summary>
        /// Tests if GetAverageFormArray method throws ArgumentException when input parameter is invalid or empty.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        [Test]
        [TestCase(new int[] { })]
        [TestCase(new int[] { 0, 1, 2, 3, 4, 5, 6 })]
        [TestCase(new int[] { 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })]
        public void GetAverageFormArrayInputIsInvalid(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentException>(() => arrayOperation.GetAverageFormArray(array));
        }

        /// <summary>
        /// Tests if GetAverageFormArray method throws ArgumentOutOfRangeException when input array values are less than 0 or greater than 1000000.
        /// </summary>
        /// <param name="array"></param>
        [Test]
        [TestCase(new int[] { 0, 1, 2, 3, 4, -5, 6, 7, 8, 9 })]
        [TestCase(new int[] { 0, 1, 2, 3, 419758649, 5, 6, 7, 8, 9 })]
        public void GetAverageFormArrayInputIsOutOfRange(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentOutOfRangeException>(() => arrayOperation.GetAverageFormArray(array));
        }

        /// <summary>
        /// Tests if GetAverageFormArray method claculates average value from input array of 10 integers.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        /// <param name="expected">Expecte average value.</param>
        [Test]
        [TestCase(new int[] { 10, 10, 20, 30, 40, 50, 60, 70, 80, 90 }, 46)]
        public void GetAverageFormArrayInputIsValid(int[] array, int expected)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            var actual = arrayOperation.GetAverageFormArray(array);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Tests if SortArrayAscending method throws ArgumentNullException when input parameter is null.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        [Test]
        [TestCase(null)]
        public void SortArrayAscendingInputIsNull(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentNullException>(() => arrayOperation.SortArrayAscending(array));
        }

        /// <summary>
        /// Tests if SortArrayAscending method throws ArgumentException when input parameter is invalid or empty.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        [Test]
        [TestCase(new int[] { })]
        [TestCase(new int[] { 0, 1, 2, 3, 4, 5, 6 })]
        [TestCase(new int[] { 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })]
        public void SortArrayAscendingInputIsInvalid(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentException>(() => arrayOperation.SortArrayAscending(array));
        }

        /// <summary>
        /// Tests if SortArrayAscending method throws ArgumentOutOfRangeException when input array values are less than 0 or greater than 1000000.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        [Test]
        [TestCase(new int[] { 0, 1, 2, 3, 4, -5, 6, 7, 8, 9 })]
        [TestCase(new int[] { 0, 1, 2, 3, 419758649, 5, 6, 7, 8, 9 })]
        public void SortArrayAscendingInputIsOutOfRange(int[] array)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            Assert.Throws<ArgumentOutOfRangeException>(() => arrayOperation.SortArrayAscending(array));
        }

        /// <summary>
        /// Tests if SortArrayAscending method return sorted array of 10 integers ascending.
        /// </summary>
        /// <param name="array">Input parameter.</param>
        /// <param name="expected">Expected array.</param>
        [Test]
        [TestCase(new int[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 }, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        [TestCase(new int[] { 15, 45, 897, 1, 13, 74, 36, 81, 102, 654}, new int[] { 1, 13, 15, 36, 45, 74, 81, 102, 654, 897})]
        public void SortArrayAscendingInputIsValid(int[] array, int[] expected)
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            var actual = arrayOperation.SortArrayAscending(array);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Tests if GetArrayWithRandomNumbers returns array of type integer with lenght of 10.
        /// </summary>
        [Test]
        public void GetArrayWithRandomNumbersValidCall()
        {
            ArrayOperations arrayOperation = new ArrayOperations();
            var actual = arrayOperation.GetArrayWithRandomNumbers();
            var actualType = actual.GetType();
            Assert.AreEqual(typeof(int[]), actualType);
            Assert.AreEqual(10, actual.Length);

        }
    }
}
